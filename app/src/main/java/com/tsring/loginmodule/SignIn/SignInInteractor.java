package com.tsring.loginmodule.SignIn;

/**
 * Created by tsering on 23/11/15.
 */
public interface SignInInteractor {
    void checkData(String first, String second);

    void checkData(String first, long second);

    void checkData(long first, String second);

    void checkData(long first, long second);
}

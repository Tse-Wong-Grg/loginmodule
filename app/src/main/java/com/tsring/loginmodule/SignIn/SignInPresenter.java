package com.tsring.loginmodule.SignIn;

/**
 * Created by tsering on 23/11/15.
 */
public interface SignInPresenter {
    void checkFirst();

    void checkSecond();

    void validateCredentials(String first, String second);
}

package com.tsring.loginmodule.SignIn;

/**
 * Created by tsering on 23/11/15.
 */
public interface SignInOnFinishListner {
    void onErrorTopField(String msg);

    void onErrorSecondField(String msg);

    void onSuccesss();

    void onError(String msg);
}

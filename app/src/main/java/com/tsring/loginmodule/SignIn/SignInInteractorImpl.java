package com.tsring.loginmodule.SignIn;

import com.tsring.loginmodule.Utils.V_DataInfo;
import com.tsring.loginmodule.Utils.Validaton;

/**
 * Created by tsering on 23/11/15.
 */
public class SignInInteractorImpl implements SignInInteractor {
    SignInOnFinishListner listner;
    SignInConfig mConfig;

    SignInInteractorImpl(SignInOnFinishListner listner) {
        this.listner = listner;
        this.mConfig = SignInConfig.getInstance();
    }

    @Override
    public void checkData(String first, String second) {
        Boolean isError = false;
        if (mConfig.getFirstType() == SignInConfig.F_EMAIL) {
            String[] checkEmail = Validaton.checkEmail(first);
            if (checkEmail[0].equals("empty")) {
                listner.onErrorTopField(checkEmail[1]);
                isError = true;
            } else if (checkEmail[0].equals("min") || checkEmail[0].equals("max") || checkEmail[0].equals("errorEmail")) {
                listner.onError(checkEmail[1]);
                isError = true;
            }
        } else {
            V_DataInfo data = Validaton.checkString(first, mConfig.getFirstMax(), mConfig.getFirstMin()).get(0);
            if (!data.isValid) {
                listner.onErrorTopField(data.msg);
                isError = true;
            }
        }
        V_DataInfo data2 = Validaton.checkString(second, mConfig.getSecondMax(), mConfig.getSecondMin()).get(0);
        if (!data2.isValid) {
            listner.onErrorSecondField(data2.msg);
            isError = true;
        }
        if (!isError) {
            if (first.equals("arjun") && second.equals("grg123"))
                listner.onSuccesss();
            else
                listner.onError("error in login");
        }
    }

    @Override
    public void checkData(String first, long second) {
        Boolean isError = false;
        if (mConfig.getFirstType() == SignInConfig.F_EMAIL) {
            String[] checkEmail = Validaton.checkEmail(first);
            if (checkEmail[0].equals("empty")) {
                listner.onErrorTopField(checkEmail[1]);
                isError = true;
            } else if (checkEmail[0].equals("min") || checkEmail[0].equals("max") || checkEmail[0].equals("errorEmail")) {
                listner.onError(checkEmail[1]);
                isError = true;
            }
        } else {
            V_DataInfo data = Validaton.checkString(first, mConfig.getFirstMax(), mConfig.getFirstMin()).get(0);
            if (!data.isValid) {
                listner.onErrorTopField(data.msg);
                isError = true;
            }
        }
        V_DataInfo data2 = Validaton.checkString(String.valueOf(second), mConfig.getSecondMax(), mConfig.getSecondMin()).get(0);
        if (!data2.isValid) {
            listner.onErrorSecondField(data2.msg);
            isError = true;
        }
        if (!isError) {
            if (first.equals("arjun") && second == 4455)
                listner.onSuccesss();
            else
                listner.onError("error in login");
        }

    }

    @Override
    public void checkData(long first, String second) {
        Boolean isError = false;
        V_DataInfo data = Validaton.checkString(String.valueOf(first), mConfig.getFirstMax(), mConfig.getFirstMin()).get(0);
        if (!data.isValid) {
            listner.onErrorTopField(data.msg);
            isError = true;
        }
        V_DataInfo data2 = Validaton.checkString(second, mConfig.getSecondMax(), mConfig.getSecondMin()).get(0);
        if (!data2.isValid) {
            listner.onErrorSecondField(data2.msg);
            isError = true;
        }
        if (!isError) {
            if (first == 9849404040L && second.equals("grg123"))
                listner.onSuccesss();
            else
                listner.onError("error in login");
        }

    }

    @Override
    public void checkData(long first, long second) {
        Boolean isError = false;
        V_DataInfo data = Validaton.checkString(String.valueOf(first), mConfig.getFirstMax(), mConfig.getFirstMin()).get(0);
        if (!data.isValid) {
            listner.onErrorTopField(data.msg);
            isError = true;
        }
        V_DataInfo data2 = Validaton.checkString(String.valueOf(second), mConfig.getSecondMax(), mConfig.getSecondMin()).get(0);
        if (!data2.isValid) {
            listner.onErrorSecondField(data2.msg);
            isError = true;
        }
        if (!isError) {
            if (first == 9849404040L && second == 4455)
                listner.onSuccesss();
            else
                listner.onError("error in login");
        }

    }
}

package com.tsring.loginmodule.SignIn;

/**
 * Created by tsering on 23/11/15.
 */
public class SignInException extends Exception {
    public SignInException(String exc) {
        super(exc);
    }

    public String getMessage() {
        return super.getMessage();
    }
}

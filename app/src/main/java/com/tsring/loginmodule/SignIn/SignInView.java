package com.tsring.loginmodule.SignIn;

import android.view.View;

/**
 * Created by tsering on 23/11/15.
 */
public interface SignInView {
    void findViews();

    void setFirstText(String label, String hint, int inputType);

    void setSecondText(String label, String hint, int inputType);

    void showErrorFirstField(String msg);

    void showErrorSecondField(String msg);

    void onSuccess(String msg);

    void onError(String msg);

    void setOnClickListners();
}

package com.tsring.loginmodule.SignIn;

import android.text.InputType;
import android.util.Log;

/**
 * Created by tsering on 23/11/15.
 */
public class SignInPresenterImpl implements SignInPresenter, SignInOnFinishListner {
    SignInView mSignInView;
    SignInInteractor mSignInInteractor;
    SignInConfig mSignInConfig;

    SignInPresenterImpl(SignInView mSignInView) {
        this.mSignInView = mSignInView;
        mSignInConfig = SignInConfig.getInstance();

        mSignInConfig = SignInConfig.getInstance();
        try {
            mSignInConfig.setFirstType(SignInConfig.F_USERNAME)
                    .setSecondType(SignInConfig.S_PIN)
                    .setFirstMax(10)
                    .setSecondMax(10)
                    .commit();
        } catch (SignInException e) {
            Log.d("Ar", e.getMessage());
        }
        mSignInInteractor = new SignInInteractorImpl(this);
        mSignInView.findViews();
        mSignInView.setOnClickListners();
        checkFirst();
        checkSecond();
    }

    @Override
    public void onErrorTopField(String msg) {
        mSignInView.showErrorFirstField(msg);
    }

    @Override
    public void onErrorSecondField(String msg) {
        mSignInView.showErrorSecondField(msg);
    }

    @Override
    public void onSuccesss() {
        mSignInView.onSuccess("Ola welcome");
    }

    @Override
    public void onError(String msg) {
        mSignInView.onError(msg);
    }

    @Override
    public void checkFirst() {
        String label = "";
        String hint = "";
        int inputType = InputType.TYPE_CLASS_TEXT;
        int type = mSignInConfig.getFirstType();
        if (type == SignInConfig.F_USERNAME) {
            label = "UserName";
            hint = "Enter user name";
        } else if (type == SignInConfig.F_EMAIL) {
            label = "Email";
            hint = "Enter Email";
            inputType = InputType.TYPE_TEXT_VARIATION_EMAIL_ADDRESS;
        } else if (type == SignInConfig.F_MOBILE) {
            label = "Mobile";
            hint = "Enter Mobile";
            inputType = InputType.TYPE_CLASS_PHONE;
        }
        mSignInView.setFirstText(label, hint, inputType);
    }

    @Override
    public void checkSecond() {
        String label = "";
        String hint = "";
        int inputType = InputType.TYPE_CLASS_TEXT;
        int type = mSignInConfig.getSecondType();
        if (type == SignInConfig.S_PASSWORD) {
            label = "Password";
            hint = "Enter password";
        } else if (type == SignInConfig.S_PIN) {
            label = "Pin";
            hint = "Enter Pin";
            inputType = InputType.TYPE_CLASS_NUMBER;
        }
        mSignInView.setSecondText(label, hint, inputType);

    }

    @Override
    public void validateCredentials(String first, String second) {
        if ((mSignInConfig.getFirstType() == SignInConfig.F_USERNAME || mSignInConfig.getFirstType() == SignInConfig.F_EMAIL) && (mSignInConfig.getSecondType() == SignInConfig.S_PASSWORD))
            mSignInInteractor.checkData(first, second);
        else if ((mSignInConfig.getFirstType() == SignInConfig.F_USERNAME || mSignInConfig.getFirstType() == SignInConfig.F_EMAIL) && (mSignInConfig.getSecondType() == SignInConfig.S_PIN)) {
            long pin;
            if (second.trim().equals(""))
                pin = 0;
            else
                pin = Long.parseLong(second);
            mSignInInteractor.checkData(first, pin);
        } else if ((mSignInConfig.getFirstType() == SignInConfig.F_MOBILE) && (mSignInConfig.getSecondType() == SignInConfig.S_PASSWORD)) {
            long mob;
            if (first.trim().equals(""))
                mob = 0;
            else
                mob = Long.parseLong(first);
            mSignInInteractor.checkData(mob, second);
        } else if (mSignInConfig.getFirstType() == SignInConfig.F_MOBILE && mSignInConfig.getSecondType() == SignInConfig.S_PIN) {
            long mob, pin;
            if (first.trim().equals(""))
                mob = 0;
            else
                mob = Long.parseLong(first);
            if (second.trim().equals(""))
                pin = 0;
            else
                pin = Long.parseLong(second);
            mSignInInteractor.checkData(mob, pin);
        }
    }
}

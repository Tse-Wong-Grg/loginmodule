package com.tsring.loginmodule.SignIn;

import android.content.Context;
import android.text.method.PasswordTransformationMethod;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.tsring.loginmodule.R;

/**
 * Created by ekbana on 11/23/15.
 */
public class SignInViewImpl implements SignInView, View.OnClickListener {
    EditText edtSecond, edtFirst;
    TextView txtSecond, txtFirst;
    Button btnSignIn;
    SignInPresenter mSignInPresenter;
    Context mContext;
    View mView;

    public SignInViewImpl(View mView, Context mContext) {
        this.mView = mView;
        this.mContext = mContext;
        mSignInPresenter = new SignInPresenterImpl(this);
    }

    @Override
    public void findViews() {
        this.edtFirst = (EditText) mView.findViewById(R.id.edtFirstField);
        this.edtSecond = (EditText) mView.findViewById(R.id.edtSecondField);
        this.txtFirst = (TextView) mView.findViewById(R.id.txtFirstField);
        this.txtSecond = (TextView) mView.findViewById(R.id.txtSecondField);
        this.btnSignIn = (Button) mView.findViewById(R.id.btnSignIn);
    }

    @Override
    public void setFirstText(String label, String hint, int inputType) {
        txtFirst.setText(label);
        edtFirst.setHint(hint);
        edtFirst.setInputType(inputType);
    }

    @Override
    public void setSecondText(String label, String hint, int inputType) {
        txtSecond.setText(label);
        edtSecond.setHint(hint);
        edtSecond.setInputType(inputType);
        edtSecond.setTransformationMethod(PasswordTransformationMethod.getInstance());
    }

    @Override
    public void showErrorFirstField(String msg) {
        edtFirst.setError(msg);
    }

    @Override
    public void showErrorSecondField(String msg) {
        edtSecond.setError(msg);
    }

    @Override
    public void onSuccess(String msg) {
        Toast.makeText(mContext, msg, Toast.LENGTH_LONG).show();
    }

    @Override
    public void onError(String msg) {
        Toast.makeText(mContext, msg, Toast.LENGTH_LONG).show();
    }

    @Override
    public void setOnClickListners() {
        btnSignIn.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        String mFirstField = "";
        String mSecondField = "";
        mFirstField = edtFirst.getText().toString().trim();
        mSecondField = edtSecond.getText().toString().trim();
        mSignInPresenter.validateCredentials(mFirstField, mSecondField);
    }
}

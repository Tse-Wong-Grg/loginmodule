package com.tsring.loginmodule.SignIn;

/**
 * Created by tsering on 23/11/15.
 */
public class SignInConfig {
    static SignInConfig sConfig;

    //TYPE FIRST
    public final static int F_USERNAME = 9990;
    public final static int F_EMAIL = 9991;
    public final static int F_MOBILE = 9992;

    //TYPE SECOND
    public final static int S_PASSWORD = 8880;
    public final static int S_PIN = 8881;

    private int F_TYPE = F_USERNAME;
    private int S_TYPE = S_PASSWORD;

    //MIN
    private int F_MIN = 4;
    private int S_MIN = 4;

    //MAX
    private int F_MAX = 10;
    private int S_MAX = 10;

    public static SignInConfig getInstance() {
        if (sConfig == null)
            sConfig = new SignInConfig();
        return sConfig;
    }

    public SignInConfig setFirstType(int type) {
        this.F_TYPE = type;
        return this;
    }

    public SignInConfig setSecondType(int type) {
        this.S_TYPE = type;
        return this;
    }

    public SignInConfig setFirstMin(int value) {
        this.F_MIN = value;
        return this;
    }

    public SignInConfig setSecondMin(int value) {
        this.S_MIN = value;
        return this;
    }

    public SignInConfig setFirstMax(int value) {
        this.F_MAX = value;
        return this;
    }

    public SignInConfig setSecondMax(int value) {
        this.S_MAX = value;
        return this;
    }

    public SignInConfig commit() throws SignInException {
        if (F_TYPE < 9990 || F_TYPE > 9992) {
            F_TYPE = F_USERNAME;
            throw new SignInException("Invalid type(should be username,email or phone)");
        }
        if (S_TYPE < 8880 || S_TYPE > 8881) {
            S_TYPE = S_PASSWORD;
            throw new SignInException("Invalid type(should be password or pin)");
        }
        return this;
    }

    public int getFirstType() {
        return F_TYPE;
    }

    public int getSecondType() {
        return S_TYPE;
    }

    public int getFirstMax() {
        return F_MAX;
    }

    public int getSecondMax() {
        return S_MAX;
    }

    public int getFirstMin() {
        return F_MIN;
    }

    public int getSecondMin() {
        return S_MIN;
    }
}

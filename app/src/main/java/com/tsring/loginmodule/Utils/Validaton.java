package com.tsring.loginmodule.Utils;

import android.util.Patterns;

import java.util.ArrayList;

/**
 * Created by ekbana on 11/9/15.
 */
public class Validaton {

    public static String[] check(String text, int min, int max) {

        // length 0 means there is no text and minimum 3 words
        if (text != null) {
            if (text.length() == 0) {
                return new String[]{"empty", text + " field is required."};
            } else if (text.length() < min)
                return new String[]{"min", text + " should not be less than " + min + " characters."};
            else if (text.length() > max)
                return new String[]{"max", text + " should not be more than " + max + " characters."};
        }
        return new String[]{"", ""};
    }

    public static ArrayList<V_DataInfo> checkString(String text, int max, int min) {
        V_DataInfo info = new V_DataInfo();
        // length 0 means there is no text and minimum 3 words
        if (text == null || text.length() == 0) {
            info.isValid = false;
            info.msg = "Empty field";
        } else if (text.length() < min) {
            info.isValid = false;
            info.msg = "Should be more than " + min + "character";
        } else if (text.length() > max) {
            info.isValid = false;
            info.msg = "Should be less than " + max + "character";
        } else
            info.isValid = true;
        ArrayList<V_DataInfo> data = new ArrayList<V_DataInfo>();
        data.add(info);
        return data;
    }

    public static String[] checkPassword(String text, int min, int max, String field) {

        // length 0 means there is no text and minimum 3 words
        if (text != null) {
            if (text.length() == 0) {
                return new String[]{"empty", field + " field is required."};
            } else if (text.length() < min)
                return new String[]{"min", field + " should not be less than " + min + " characters."};
            else if (text.length() > max)
                return new String[]{"max", field + " should not be more than " + max + " characters."};
        }
        return new String[]{"", ""};
    }

    public static String[] checkEmail(String email) {
        if (email != null) {
            if (email.length() == 0) {
                return new String[]{"empty", email + " field is required."};
            } else if (!Patterns.EMAIL_ADDRESS.matcher(email).matches()) {
                return new String[]{"errorEmail", email + " should be valid email."};
            }
        }
        return new String[]{"", ""};
    }

    public static String[] checkConfirmPassword(String confirmPassword, String password) {
        if (password != null && confirmPassword != null) {
            if (password.equals("") && confirmPassword.equals("")) {
                return new String[]{"misMatch", "field is required."};
            } else if (!password.equals(confirmPassword)) {
                return new String[]{"misMatch", "Password should be same."};
            }
        }
        return new String[]{"", ""};
    }
}
